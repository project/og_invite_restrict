DESCRIPTION
--------------------------
Enable site admins to limit group invites to current users only.

INSTALLATION
---------------
- Organic Groups module is required. 
- Enable og_invite_restrict module.
- Give "access user profiles" permissions to anyone who has access to the OG invite form.
- That's it!

TODO/BUGS/FEATURE REQUESTS
----------------
- see http://drupal.org/project/issues/og_invite_restrict

CREDITS
----------------------------
Authored and maintained by michael anello <manello AT gmail DOT com>
Sponsored by Ozmosis - http://www.ozmosis.com/
